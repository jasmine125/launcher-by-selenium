#from copy import deepcopy
import json


JSON_FILE = 'setting.json'


class Data:
    """
    설정값을 읽어 처리하는 클래스
    """

    def __init__(self):
        """
        생성자
        """
        self.__load()

    def __load(self):
        try:
            fjson = open(JSON_FILE, 'r', encoding='utf-8')
            self.data = json.load(fjson)
        except FileNotFoundError as fnfe:
            print('*** Cannot found the setting.json! ***')
            raise fnfe
        except Exception as e:
            print('*** Cannot load the setting.json! ***')            
            raise e
        finally:
            fjson.close()

    def get_version(self) -> str:
        """
        setting.json의 버전 정보를 얻는다.

        @Return 버전
        """
        return self.data['version']

    def get_regions(self) -> list:
        """
        모든 지역 정보를 얻는다.

        @Return list({지역, 홈페이지, ID, PWD})
        """
        return self.data['region']

    def get_setting(self, name:str) -> dict:
        """
        지역정보를 반환한다.
            
        @Args name 지역 이름
        @Return 지역 정보 dictionary
        """
        for item in self.get_regions():
            if item['name'] == name:
                return item
        return None
    
    def get_chrome_path(self) -> str:
        """
        지역정보를 반환한다.
            
        @Return Chrome 실행경로 패스
        """
        return self.data['chrome']


if __name__ == "__main__":
    db = Data()
    v = db.get_version()
    r = db.get_regions()
    print(v)
    print(r[1])
    setting = db.get_setting('지역1')
    print(setting)
    

