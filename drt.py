import sys
from data import Data
import navigator
    

class Customer:
    """
    고객 정보 class
    """

    def __init__(self, sta:str, dst:str, name:str, phone:str):
        """
        생성자

        @Args sta 출발지
        @Args dst 도착지
        @Args name 닉네임
        @Args phone 휴대폰 번호
        """
        self.sta = sta
        self.dst = dst
        self.name = name
        self.phone = phone


class Region:
    """
    지역 class
    """

    def __init__(self, **kwargs):
        """
        생성자
        
        @Kwargs title DRT 이름
        @Kwargs name 지역 이름
        @Kwargs login 로그인 url
        @Kwargs url 배차 url
        @Kwargs id ID
        @Kwargs pwd Password
        """
        self.kwargs = kwargs

    def __getitem__(self, key:str) -> str:
        return self.kwargs[key]
    


if __name__ == '__main__':
    try:
        loc = sys.argv[1]
    except:
        loc = '지역1'
    
    try:
        start = sys.argv[2]
    except:
        start = ''
    
    try:
        end = sys.argv[3]
    except:
        end = ''

    try:
        name = sys.argv[4]
    except:
        name = ''

    try:
        phone = sys.argv[5]
    except:
        phone = ''

    db = Data() # load setting.json
    region = Region(**db.get_setting(loc))   # 지역 정보
    path = db.get_chrome_path()
    customer = Customer(start, end, name, phone)
    
    navigator.navigator(path, region, customer)
    # navigator.navigator2(region, customer)
        