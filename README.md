# Selenium Launcher



## 개요

특정 사이트에 접속하여 로그인 후 필요 항목들의 값을 자동으로 지정하도록 도와주는 유틸리티이다.

## 환경

Python 3.11로 작성되어 있으며, selenium으로 자동화 동작한다.
webdriver_manager는 chromedriver를 자동으로 설치해주기 위해서 사용한다.

```
$ pip install requests
$ pip install selenium
```
- [Selenium API document](https://selenium-python.readthedocs.io/)


## setting.json

사이트에 대한 정보가 들어가 있기 때문에 사이트가 추가되거나 변경이 될때 반드시 수정이 필요하다.

- "version": 버전
  - 내용이 바뀌면 버전도 업데이트 하도록 하자.
- "chrome": 크롬 설치 경로
  - ![경로 확인 방법](./asset/img.png)
- "region": 지역 정보
  - title: 지역 타이틀
  - name: 하위 지역 이름
  - login: 로그인 URL
  - url: 메인 URL
  - id: ID
  - pwd: PWD

## 실행방법
```
C:\utility> launcher.exe "하위 지역" "번호" "번호" "닉네임" "휴대폰 번호"
```
잘못된 "하위 지역"값이 지정되는 경우 에러가 발생한다.


## 설치 파일

### 설치 파일 만들기
[Inno setup](https://jrsoftware.org)으로 만들 수 있으며, [빌드 스크립트](./setup/setup.iss)는 이미 작성되어 있기 때문에 inno setup만 설치되어 있다면 [빌드 스크립트](./setup/setup.iss) 실행후 컴파일(Ctrl+F9)만 하면 된다.
설치 파일 배포시 버전을 올리는 것을 권장한다.

#define MyAppVersion "1.0"

### 설치 파일 배포
./setup/Output/launcher v1.0.exe

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.