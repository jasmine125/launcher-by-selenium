import json
import requests
import subprocess
from drt import Customer, Region
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
# from selenium.webdriver.common.by import By
# from webdriver_manager.chrome import ChromeDriverManager
import tkinter.messagebox as tkmb
from tkinter import *


PROFILE = r'C:\remote-profile'  # Profile path
PORT = 9222 # Remote debugging port number


def login(region:Region) -> requests.models.Response:
    """
    로그인 성공 후 response를 얻는다.

    @Args region 지역 정보(Region instance)
    @Return Response
    """
    # 로그인 세션 생성
    url = region['login']
    id = region['id']
    pwd = region['pwd']
    url = f'{url}/loginProcess?id={id}&pw={pwd}&remember-me=on'
    session = requests.Session()
    response = session.post(url)
    try:
        json_object = json.loads(response.content)
    except json.JSONDecodeError as jde:
        tkmb.showwarning('에러', '로그인에 실패하였습니다.')
        return
    
    if response.status_code == 200 and json_object['resultCode'] == 'E0000':
        print('로그인 성공')
    else:
        tkmb.showwarning('에러', '로그인에 실패하였습니다.')
        # raise RuntimeError(f'로그인 실패. status code is {response.status_code}.')
        return
    
    return response

def set_cookies(wd:webdriver, r:requests.models.Response) -> None:
    """
    Requests의 쿠키 설정. 

    @Args wd WebDriver
    @Args r Requests의 response로 쿠키가 있어야 한다.
    """
    for cookie in r.cookies:
        cookie_dict = {"domain": cookie.domain, "name": cookie.name, "value": cookie.value, "secure": cookie.secure}
        if cookie.expires:
            cookie_dict["expires"] = cookie.expires
        if cookie.path_specified:
            cookie_dict["path"] = cookie.path
        wd.add_cookie(cookie_dict)

def navigator(path: str, region:Region, customer:Customer) -> None:
    """
    배차 화면으로 이동

    @Args path chrome 실행파일 경로
    @Args region 지역 정보(Region instance)
    @Args customer 고객 정보(Customer instance)
    """
    if region == None:
        print('Region is none.')
        return
    
    reponse = login(region)
    if reponse == None:
        return

    # 9222 포트로 실행중인 브라우저가 있는지 체크하는 방법 추가
    cmd = f'netstat -ano | findstr 127.0.0.1:{PORT} | findstr LISTENING'
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    out, err = proc.communicate()
    
    # out 정보가 있다는건 9222 포트를 디버그용으로 사용중인 프로그램이 있다는 뜻
    if len(out) <= 0: # 9222 포트가 없으니 브라우저를 실행한다.
        cmd = path
        cmd += f' --user-data-dir="{PROFILE}" --remote-debugging-port={PORT}'
        subprocess.Popen(cmd)

    # 크롬 드라이버 생성
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option('debuggerAddress', f'127.0.0.1:{PORT}')
    # 크롬이 업데이트 되면서 ChromeDriverManager().install()에서 에러 발생. service param 삭제
    # https://stackoverflow.com/questions/76727774/selenium-webdriver-chrome-115-stopped-working
    # driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), 
    #                          options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(region['login']) # URL로 이동
    set_cookies(driver, reponse)    # 세션 쿠키 복사
    # 불필요한 크롬창 제거 --> 더 이상 필요 없어진 코드
    # if len(driver.window_handles) > 1:  # handle의 순서는 최근에서 오래된 순서로 됨
    #     handle = driver.window_handles[0]   # 가장 최근에 생긴 빈 chrome
    #     driver.switch_to.window(handle)
    #     driver.close()  # 가장 최근에 생긴 빈 chrome을 닫는다.
    # driver.switch_to.window(driver.window_handles[-1])  # 가장 오래된 handle로 전환
    # driver.get(region['login']) # URL로 이동
    # set_cookies(driver, reponse)    # 세션 쿠키 복사

    # GET 방식으로 필수 element 값 설정
    url = region['url']
    url += f'&staAddress={customer.sta}&dstAddress={customer.dst}'
    url += f'&nickName={customer.name}&phoneNo={customer.phone}'
    driver.get(url) # URL로 이동

    # END
    print('end')


# def navigator2(region:Region, customer:Customer) -> None:
#     """
#     배차 화면으로 이동

#     @Args setting Setting instance
#     @Args sta 출발지
#     @Args dst 도착지
#     @Args name 닉네임
#     @Args phone 휴대폰번호
#     """
#     if region == None:
#         print('Region is none.')
#         return
    
#     url = region['url']
#     response = login(region)

#     # 크롬 드라이버 생성
#     chrome_options = webdriver.ChromeOptions()
#     chrome_options.add_argument(f'user-data-dir={PROFILE}')  # User Data 생성
#     chrome_options.add_argument(f'remote-debugging-port={PORT}')
#     chrome_options.add_experimental_option('detach', True)
#     driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

#     # 불필요한 크롬창 제거
#     if len(driver.window_handles) > 1:
#         for handle in driver.window_handles[:-1]:
#             driver.switch_to.window(handle)
#             driver.close()
#     driver.switch_to.window(driver.window_handles[0])
#     driver.get(url) # URL로 이동
#     # set_cookies(driver, response)

#     # GET 방식으로 필수 element 값 설정
#     url += f'&staAddress={customer.sta}&dstAddress={customer.dst}&nickName={customer.name}&phoneNo={customer.phone}'
#     driver.get(url) # URL로 이동

#     # XPath로 element를 찾아 값 설정
#     # start = driver.find_element(By.XPATH, '//*[@id="staAddress"]')
#     # start.send_keys(sta)

#     # destination = driver.find_element(By.XPATH, '//*[@id="dstAddress"]')
#     # destination.send_keys(dst)

#     # nickname = driver.find_element(By.XPATH, '//*[@id="nickName"]')
#     # nickname.send_keys(name)

#     # phoneno = driver.find_element(By.XPATH, '//*[@id="phoneNo"]')
#     # phoneno.send_keys(phone)

#     # END
#     print('end')
